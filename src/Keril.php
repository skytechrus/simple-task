<?php

namespace Skytech;
/**
 * Class Keril
 */
class Keril
{
    /**
     * @var
     */
    public $i;
    /**
     * @var
     */
    public $j;

    /**
     * @param $i
     * @param $j
     * @return mixed
     */
    public function multiply($i, $j)
    {
        return $i*$j;
    }

    /**
     * @param $e
     * @param $d
     * @return float|int
     */
    public function delenie($e, $d)
    {
        return $d/$e;
    }
}