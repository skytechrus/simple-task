FROM php:5.6.31-fpm-alpine
COPY . /usr/src/simple-task
WORKDIR /usr/src/simple-task
CMD [ "php", "./php.php" ]