<?php

use Skytech\Keril;

/**
 * Class KerilTest
 */
class KerilTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    /**
     * @var Keril
     */
    private $trololo;

    /**
     *
     */
    protected function _before()
    {
        $this->trololo = new Keril();
    }

    /**
     *
     */
    protected function _after()
    {
    }

    // tests

    /**
     * @dataProvider dataProvider
     */
    public function testMultiply($expect, $a, $b)
    {
        $this->assertEquals($expect, $this->trololo->multiply($a,$b));
    }

    public function dataProvider()
    {
        return array(
            array(4,2,2),
            array(6,2,3)
        );
    }
}